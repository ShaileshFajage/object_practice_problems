

let givenArray = require(`./array`);

function salaryValuesIntoNumbers(arr) {
    
    for(let i=0;i<arr.length;i++)
    {
        let sal = arr[i].salary;
        sal = sal.replace(/[$]/,'');
        arr[i].salary =  Number(sal);
        
    }

    return arr;

}

console.log(salaryValuesIntoNumbers(givenArray));